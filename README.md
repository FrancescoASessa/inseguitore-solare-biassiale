# Inseguitore Solare Biassiale
## Realizzato da Francesco A. Sessa
### Progetto Maturit� a.s. 2014-2015
#### Necessario:

- 1x Arduino
- 2x Servo
- 4x Fotoresistenze LDR
- 4x Resistenze 4,7 Kohm

- - -

**Grazie** [Share MakerSpace](http://www.sharemakers.it/) per l'aiuto nella realizzazione pratica e per il supporto.

- - -

Quest'opera � distribuita con [Licenza Creative Commons Attribuzione - Non commerciale 4.0 Internazionale.](http://creativecommons.org/licenses/by-nc/4.0/)