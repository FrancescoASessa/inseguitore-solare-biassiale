// Sistema di controllo Inseguitore Solare Biassiale
// Francesco Sessa 5^A
#include <Servo.h>     // Includo la libreria Servo

Servo orizzontale;     // Servo Orizzontale
int servoh = 90;     // Posiziono il Servo Orizzontale a 90°
int o_pin = 11;      // Pin del servo Orizzontale

Servo verticale;       // Servo Verticale
int servov = 0;     // Posiziono il Servo Verticale a 90°
int v_pin = 9;      // Pin del servo verticale

// Variabili delle fotoresistenze (LDR)
int ldrlt = 0; //LDR alto a sinistra
int ldrrt = 2; //LDR alto a destra
int ldrld = 1; //LDR basso a sinistra
int ldrrd = 3 ; //LDR basso a destra

//Variabili Generiche
  int dtime = 10; //Variabile del Delay 
  int tol = 256;   //Variabile della Tolleranza
  //int tol = analogRead(A5);
void setup()
{
  Serial.begin(9600);
// Connetto i servo
  orizzontale.attach(o_pin); 
  verticale.attach(v_pin);
  orizzontale.write(servoh);
  verticale.write(servov);
  
}

void loop() 
{
  // Serial.println(tol);
  //Leggo i valori analogici delle Fotoresistenze
  int lt = analogRead(ldrlt); // Alto a Sinistra
  int rt = analogRead(ldrrt); // Alto a Destra
  int ld = analogRead(ldrld); // Basso a Sinistra
  int rd = analogRead(ldrrd); // Basso a Destra
  
  // Faccio la media dei valori delle fotoresistenze
  int avt = (lt + rt) / 2; //  Su
  int avd = (ld + rd) / 2; // Giu
  int avl = (lt + ld) / 2; //  Sinistra
  int avr = (rt + rd) / 2; // Destra

  int dvert = avt - avd; // Verifico la differenza tra la fotoresistenza superiore e quella inferiore
  int dhoriz = avl - avr;// Verifico la differenza tra la fotoresistenza di sinistra e di destra
    
  if (-1*tol > dvert || dvert > tol) // Controlla se la differenza e nella tolleranza altrimenti cambio l'angolo del servo verticale
  {
  if (avt > avd)
  {
    servov = --servov;
     if (servov > 180) 
     { 
      servov = 180;
     }
  }
  else if (avt < avd)
  {
    servov= ++servov;
    if (servov < 0)
  {
    servov = 0;
  }
  }
  verticale.write(servov);
  }
  
  if (-1*tol > dhoriz || dhoriz > tol) // Controlla se la differenza e nella tolleranza altrimenti cambio l'angolo del servo Orizzontale
  {
  if (avl > avr)
  {
    servoh = --servoh;
    if (servoh < 0)
    {
    servoh = 0;
    }
  }
  else if (avl < avr)
  {
    servoh = ++servoh;
     if (servoh > 180)
     {
     servoh = 180;
     }
  }
  else if (avl = avr)
  {
    // Nulla
  }
  orizzontale.write(servoh);
  }
   delay(dtime); 
}
